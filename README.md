<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# My ZSH Config

Hello and welcome to my custom configuration for the ZSH shell (best shell)!
This configuration is tailored for my needs. Add or remove whatever you want,
just understand that this configuration is very hands on.

You may notice that this configuration uses git submodules for plugin
management. Why? because this configuration is tightly linked to my rice setup.
Any changes made to the rice leads to changes in this configuration, so
managing dependencies through git is easier and more flexible.

## Installation

Just clone and run `make install`. View the [installation file][install-file]
for more details about installing my configuration.

I recommend you cherry pick what you want into your configuration rather than
just copying it. Understand that this configuration was made for me, and not
for the public at large. Therefore, things may break or seem weird if you do
not understand the purpose of it outright.

## Contributing

My ZSH configuration is opinionated, but not stubborn. This project is open to
the following forms of contribution:

- Bug reports and fixes.
- Feature requests and implementations.
- Improvments to documentation.

View the [contributing file][contributing-file] for some guidelines about
contribution. Any help or advice will be appreciated regardless.

## License

View the [license file][license-file] or view the contents of the
[LICENSES][license-dir] directory for the legal stuff.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a><br/>
This document is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[contributing-file]: https://gitlab.com/rice-config/my-zsh-config/blob/main/CONTRIBUTING.md
[install-file]: https://gitlab.com/rice-config/my-zsh-config/blob/main/INSTALL.md
[license-file]: https://gitlab.com/rice-config/my-zsh-config/blob/main/LICENSE.md
[license-dir]: https://gitlab.com/rice-config/my-zsh-config/blob/main/LICENSES
