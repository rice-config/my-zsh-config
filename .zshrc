# SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
# SPDX-License-Identifier: ISC

# General configuration options...
setopt interactive_comments
setopt CORRECT
setopt CORRECT_ALL
setopt AUTO_CD
setopt EXTENDED_HISTORY
stty stop undef
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=$HISTFILE

# Load auto completion configruations...
[[ -f "$ZDOTDIR/scripts/completions.zsh" ]] &&
  source "$ZDOTDIR/scripts/completions.zsh"

# Load aliases...
[[ -f "$ZDOTDIR/.zalias" ]] && source "$ZDOTDIR/.zalias"

# Load vi mode plugin.
function zvm_config() {
  ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT
  ZVM_VI_INSERT_ESCAPE_BINDKEY=jj
}

[[ -f "$ZDOTDIR/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh" ]] &&
  source "$ZDOTDIR/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh"

# Load tmux plugin...
[[ -f "$ZDOTDIR/plugins/tmux/tmux.plugin.zsh" ]] &&
  source "$ZDOTDIR/plugins/tmux/tmux.plugin.zsh"

# Load git prompt plugin...
[[ -f "$ZDOTDIR/plugins/git-prompt.zsh/git-prompt.zsh" ]] &&
  source "$ZDOTDIR/plugins/git-prompt.zsh/git-prompt.zsh"

[[ -f "$ZDOTDIR/scripts/rprompt.zsh" ]] &&
  source "$ZDOTDIR/scripts/rprompt.zsh"

# Load K plugin...
[[ -f "$ZDOTDIR/plugins/k/k.sh" ]] &&
  source "$ZDOTDIR/plugins/k/k.sh"

# Load syntax highlighting plugin...
[[ -f "$ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh" ]] &&
  source "$ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh"
