<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

_Thanks for contributing!_

The following should not be considered steadfast rules, but simple guidelines
that require your best judgement at all times.

## Expected contributions

My ZSH configuration is opinionated, but not stubborn. This project is open to
the following forms of contribution:

- Bug reports and fixes.
- Feature requests and implementations.
- Improvments to documentation.

## Git branch structure

This project abides by [GitLab Flow][gitlab-flow]. All development occurs off
of main such that main should never be broken. Feature branches is where
pull requests occur, and should be short lived. Do not attempt to create a
feature branch from a feature branch, base everything on main.

## Coding style

1. Indentation is two spaces.
2. Maximum column limit is 80 characters.

## Commit style

This project uses [Conventional Commits][conv-commits]. All commits must be
signed off to abide by the [Linux Developer Certificate of Origin][linux-dco].

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a><br/>
This document is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[gitlab-flow]: https://cm-gitlab.stanford.edu/help/workflow/gitlab_flow.md
[conv-commits]: https://www.conventionalcommits.org/en/v1.0.0/
[linux-dco]: https://elinux.org/Developer_Certificate_Of_Origin
