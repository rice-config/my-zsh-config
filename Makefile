# SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
# SPDX-License-Identifier: ISC

all: update

update:
	git submodule foreach git pull
	git commit -qsm "chore(plugin): Update plugins"

install:
	git submodule update --init --recursive
	mkdir -p ~/.config/zsh/
	cp -rf . ~/.config/zsh/
	ln -s ~/.config/zsh/.zshenv ~/.zshenv

uninstall:
	rm -rf ~/.config/zsh/
	rm -rf ~/.zshenv

.PHONEY: all update install uninstall
