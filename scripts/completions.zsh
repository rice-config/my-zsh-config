#!/bin/zsh

# SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
# SPDX-License-Identifier: ISC

# Auto completion configurations.

# Load ZSH completions plugin...
fpath=($ZDOTDIR/plugins/zsh-completions/src $fpath)

# Setup completions...
zmodload zsh/complist
autoload -U compinit
compinit -d $ZSH_COMPDUMP
_comp_options+=(globdots)

# Menu selection keys...
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'c' clear-screen
bindkey -M menuselect 'y' accept-line
bindkey -M menuselect 'u' undo
bindkey -M menuselect 'n' accept-and-hold 

# Options...
setopt AUTO_MENU
setopt AUTO_PARAM_SLASH

# Setup cache...
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$ZSH_COMPCACHE"

# Define completers...
zstyle ':completion:*' completer _extensions _complete _approximate

# Allow selection in menu...
zstyle ':completion:*' menu select

# Colorize completion menu...
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
