#!/bin/zsh

# SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
# SPDX-License-Identifier: ISC

# Add some color to our commands.

if ! [ -x "$(command -v grc)" ]; then
  echo "Cannot find grc for colorization"
  return
fi

cmds=(
  as
  blkid
  cc
  configure
  curl
  df
  diff
  dig
  du
  env
  fdisk
  findmnt
  free
  g++
  gas
  gcc
  getfacl
  getsebool
  gmake
  id
  ifconfig
  iostat
  ip
  iptables
  iwconfig
  journalctl
  last
  ldap
  lolcat
  ld
  ls
  lsattr
  lsblk
  lsmod
  lsof
  lspci
  make
  mount
  mtr
  netstat
  nmap
  ntpdate
  ping
  ping6
  proftpd
  ps
  sar
  semanage
  sensors
  showmount
  sockstat
  ss
  stat
  sysctl
  systemctl
  tcpdump
  traceroute
  traceroute6
  tune2fs
  ulimit
  uptime
  vmstat
  wdiff
  whois
)

for cmd in $cmds; do
  if (( $+commands[$cmd] )); then
    $cmd() {
      grc --colour=auto ${commands[$0]} "$@"
    }
  fi
done
unset cmds cmd
