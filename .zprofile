# SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
# SPDX-License-Identifier: ISC

# Connect main configuration...
[[ -f "$ZDOTDIR/.zshrc" ]] && source "$ZDOTDIR/.zshrc"

# Connect to GPG SSH agent...
[[ -f "$ZDOTDIR/scripts/gpg-ssh-agent.zsh" ]] &&
  source "$ZDOTDIR/scripts/gpg-ssh-agent.zsh"
