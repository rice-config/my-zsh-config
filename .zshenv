# SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
# SPDX-License-Identifier: ISC

# Add ~/.local/bin to $PATH...
export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}"

# XDG base directory setup. Reference
# <https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>.
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# Application environment variables...
export EDITOR="vim"
export TERMINAL="st"
export BROWSER="firefox"

# Configuration files...
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export GNUPGHOME="${XDG_CONFIG_HOME:-$HOME/.config}/gnupg"
export VIMINIT='let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc" | source $MYVIMRC'
export XINITRC="${XDG_CONFIG_HOME:-$HOME/.config}/x11/xinitrc"

# Data files...
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"

# State files...
export HISTFILE="${XDG_STATE_HOME:-$HOME/.local/state}/zsh/history"

# Cache files...
export ZSH_COMPDUMP="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/.zcompdump-$HOST"
export ZSH_COMPCACHE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/.zcompcache-$HOST"
